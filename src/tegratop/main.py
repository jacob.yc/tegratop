import subprocess
from rich.bar import Bar
from rich.table import Table
from rich.live import Live
from rich.console import Group
from rich.panel import Panel
import re

def get_color(usage):
    if usage > 80:
        return "red"
    elif usage > 50:
        return "yellow"
    else:
        return "green"

def main():
    # Start the subprocess
    process = subprocess.Popen(
        ["tegrastats"],
        stdout=subprocess.PIPE,
        universal_newlines=True
    )

    try:
        # for line in process.stdout:
        #     stats = line.strip()

        #     matches = re.findall(r'C(\d+):(\d+)%@(\d+)', stats)

        #     # Convert all strings to integers and sort by the first element (XX)
        #     result = sorted([(int(xx), int(yy), int(zz)) for xx, yy, zz in matches], key=lambda x: x[0])

        #     print(result, stats)

        with Live(screen=True) as live:
            for line in process.stdout:
                stats = line.strip()
                items = []

                datetime = " ".join(stats.split(" ")[:2])
                items.append(datetime)

                pattern = r"CPU \[.*\]"
                match = re.search(pattern, stats)
                if match:
                    table = Table(show_header=False)

                    pattern = r"(\d+)%@(\d+)"
                    matches = re.findall(pattern, match.group())
                    values = [(int(a), int(b)) for a, b in matches]
                    for i, cpu in enumerate(values):
                        table.add_row(f"CPU {i}", f"{cpu[1]} MHz", Bar(size=101, begin=0, end=cpu[0]+1, color=get_color(cpu[0])), f"{cpu[0]}%")

                    items.append(table)

                matches = re.findall(r'C(\d+):(\d+)%@(\d+)', stats)
                result = sorted([(int(xx), int(yy), int(zz)) for xx, yy, zz in matches], key=lambda x: x[0])
                if result:
                    table = Table(show_header=False)

                    for cpu_id, util, freq in result:
                        table.add_row(f"CPU {cpu_id}", f"{freq} MHz", Bar(size=101, begin=0, end=util+1, color=get_color(util)), f"{util}%")
                    
                    items.append(table)
                
                pattern = r"RAM (\d+)/(\d+)MB"
                match = re.search(pattern, stats)
                if match:
                    table = Table(show_header=False)

                    used_ram = int(match.group(1))
                    total_ram = int(match.group(2))
                    ram_usuage = used_ram / total_ram * 100
                    table.add_row(f"RAM", Bar(size=total_ram, begin=0, end=used_ram, color=get_color(ram_usuage)), f"{used_ram}/{total_ram}MB")
                    
                    items.append(table)

                pattern = r"GR3D_FREQ (\d+)%(@(\d+))?"
                match = re.search(pattern, stats)
                if match:
                    table = Table(show_header=False)

                    gpu_util = int(match.group(1))
                    gpu_freq = int(match.group(3)) if match.group(3) else None

                    if gpu_freq:
                        table.add_row(f"iGPU-GPC0", f"{gpu_freq} MHz", Bar(size=101, begin=0, end=gpu_util+1, color=get_color(gpu_util)), f"{gpu_util}%")
                    else:
                        table.add_row(f"iGPU-GPC0", "", Bar(size=101, begin=0, end=gpu_util+1, color=get_color(gpu_util)), f"{gpu_util}%")

                    pattern = r"GR3D2_FREQ (\d+)%@(\d+)"
                    match = re.search(pattern, stats)
                    if match:
                        gpu_util = int(match.group(1))
                        gpu_freq = int(match.group(2))

                        table.add_row(f"iGPU-GPC1", f"{gpu_freq} MHz", Bar(size=101, begin=0, end=gpu_util+1, color=get_color(gpu_util)), f"{gpu_util}%")
                    
                    pattern = r"(NVDLA0_LOAD|NVDLA0)\s+(\d+)%@?(\d+)?"
                    match = re.search(pattern, stats)
                    if match:
                        nvdla0_load = int(match.group(2))
                        nvdla0_freq = int(match.group(3) or 0)

                        table.add_row(f"DLA 0", f"{nvdla0_freq} MHz", Bar(size=101, begin=0, end=nvdla0_load+1, color=get_color(nvdla0_load)), f"{nvdla0_load}%")
                    
                    pattern = r"(NVDLA1_LOAD|NVDLA1)\s+(\d+)%@?(\d+)?"
                    match = re.search(pattern, stats)
                    if match:
                        nvdla1_load = int(match.group(2))
                        nvdla1_freq = int(match.group(3) or 0)

                        table.add_row(f"DLA 1", f"{nvdla1_freq} MHz", Bar(size=101, begin=0, end=nvdla1_load+1, color=get_color(nvdla1_load)), f"{nvdla1_load}%")
                    
                    pattern = r"NVENC_LOAD (\d+)% NVDEC_LOAD (\d+)%"
                    match = re.search(pattern, stats)
                    if match:
                        enc_load = int(match.group(1))
                        dec_load = int(match.group(2))

                        table.add_row(f"Video Enc", "", Bar(size=101, begin=0, end=enc_load+1, color=get_color(enc_load)), f"{enc_load}%")
                        table.add_row(f"Video Dec", "", Bar(size=101, begin=0, end=dec_load+1, color=get_color(dec_load)), f"{dec_load}%")

                    items.append(table)

                matches = re.findall(r'\b([A-Za-z0-9]+)@([0-9\.]+)C\b', stats)
                result = [(name, float(temp)) for name, temp in matches]
                if result:
                    table = Table(show_header=False)

                    for name, temp in result:
                        table.add_row(name, Bar(size=101, begin=0, end=temp+1, color=get_color(temp)), f"{temp}\u00b0C")
                    
                    items.append(table)

                live.update(Group(*items))


    except KeyboardInterrupt:
        # Handle keyboard interrupt
        process.terminate()
        process.wait()

    finally:
        # Cleanup resources
        process.stdout.close()

if __name__ == "__main__":
    main()
