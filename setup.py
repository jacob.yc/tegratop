from setuptools import setup, find_packages

setup(
    name='tegratop',
    version='1.0.0',
    author='Jacob YC',
    author_email='jacob.yc.wang@foxconn.com',
    description='tegratop',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'tegratop = tegratop.main:main'
        ]
    },
    install_requires=[
        'rich==13.4.2',
    ],
)
